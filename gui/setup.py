#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec  9 11:29:41 2016
Setup skript for installing the primo_gui and it's dependencies.
@author: jpoeppel, koberger
"""

from setuptools import setup
import sys

if sys.argv[-1] == 'setup.py':
    print("To install, run 'python setup.py install'")
    print()

version = 0.5

setup(name="primo_gui",
      version=version,
      description="Prototypical visualisation for bayesian networks in the browser.",
      long_description="This project is an experimental implvementation of a " \
                       "visualisation for bayesian networks stored in xbif files. " \
                       "It has been developed to work with the reimplementation of "\
                       "PRIMO, allowing the visualization of the graph and its "\
                       "parameters as well as manipulation of both the structure "\
                       "and the parameters. Furthermore, it allows to directly make "\
                       "inferences in the browser. This implementation was inspired "\
                       "by the nengo_gui: https://github.com/nengo/nengo_gui, but is "\
                       "implemented very differently.",
      author="Jan Pöppel",
      author_email="jpoeppel@techfak.uni-bielefeld.de",
      packages = ["primo_gui"],
      include_package_data=True,
      zip_safe=False,
      scripts=["runPrimo_gui.py"],
      install_requires=["flask"])
