import primo_gui.inferenceMethods as iM


class GuiGraph:
    def __init__(self, graph_mode=None):
        if graph_mode is None:
            graph_mode = ["BN", ""]
        self._width = 0
        self._height = 0
        self._node_radius = 50
        self._graph = {}
        self._dir = ""
        self._nr = 0
        self._current_inference_method = ""
        self._current_layout = ""
        self._is_first_position_update = True
        self._graph_mode = graph_mode
        if self._graph_mode[0] == "BN":
            self._inference_methods = iM.InferenceMethodsBN()
        elif self._graph_mode[0] == "DBN":
            self._inference_methods = iM.InferenceMethodsDBN()

    @property
    def width(self):
        return self._width

    @width.setter
    def width(self, value):
        self._width = value

    @property
    def height(self):
        return self._height

    @height.setter
    def height(self, value):
        self._height = value

    @property
    def graph(self):
        return self._graph.copy()

    @graph.setter
    def graph(self, value):
        self._graph = value.copy()

    @property
    def nr(self):
        return self._nr

    @nr.setter
    def nr(self, value):
        self._nr = value

    def get_next_nr(self):
        self._nr += 1
        return self._nr

    @property
    def current_inference_method(self):
        return self._current_inference_method

    @current_inference_method.setter
    def current_inference_method(self, value):
        self._current_inference_method = value

    @property
    def current_layout(self):
        return self._current_layout

    @current_layout.setter
    def current_layout(self, value):
        self._current_layout = value

    @property
    def inference_methods(self):
        return self._inference_methods

    @inference_methods.setter
    def inference_methods(self, value):
        self._inference_methods = value

    @property
    def is_first_position_update(self):
        return self._is_first_position_update

    @is_first_position_update.setter
    def is_first_position_update(self, value):
        self._is_first_position_update = value

    @property
    def node_radius(self):
        return self._node_radius

    @node_radius.setter
    def node_radius(self, value):
        self._node_radius = value

    @property
    def dir(self):
        return self._dir

    @dir.setter
    def dir(self, value):
        self._dir = value

    @property
    def graph_mode(self):
        return self._graph_mode

    @graph_mode.setter
    def graph_mode(self, value):
        self._graph_mode = value

    def new_graph(self, graph_mode=None):
        if graph_mode is None:
            graph_mode = ["BN", ""]
        self._graph_mode = graph_mode
        self._graph = {}
        self._nr = 0
        self._current_inference_method = ""
        self._current_layout = ""
        self._is_first_position_update = True
        if self._graph_mode[0] == "BN":
            self._inference_methods = iM.InferenceMethodsBN()
        elif self._graph_mode[0] == "DBN":
            self._inference_methods = iM.InferenceMethodsDBN()
        self._graph = {}
