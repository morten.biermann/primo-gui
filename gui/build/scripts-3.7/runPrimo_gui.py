#!C:\Users\morte\Anaconda3\python.exe
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 26 10:05:34 2017

@author: jpoeppel
"""

import argparse

from primo_gui import app

# initial:
parser = argparse.ArgumentParser()
parser.add_argument('-P', '--port', dest='port', metavar='PORT', default=8080, type=int, help='port to run server on')
parser.add_argument('filename', nargs='?', type=str, help='initial .xbif or .conf to load')
parser.add_argument('--debug', action='store_true', help='turn on debug logging')
parser.add_argument('--browser', dest='browser', action='store_true')
parser.add_argument('--no-browser', dest='browser', action='store_false')
parser.set_defaults(browser=True)
args = parser.parse_args()

if args.browser:
    import webbrowser
    import threading

    threading.Thread(target=webbrowser.open, args=('http://localhost:%d' % args.port,)).start()

app.filename = args.filename

app.run(port=args.port, debug=args.debug)