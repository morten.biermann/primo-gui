##Experimental PRIMO Visualisation

Uses d3.js as frontend to render the graph in a webbrowser. Currently flask is used as a simple webserver that handles the communication between Python and Javascript.

##Requirements:

The minimal requirements are listed in requirements.txt and can be easily installed using 

pip install -r requirements.txt

After that you can use 

python setup.py [install,develop]

to install the GUI.


##Start:

The visualisation can be run by using the script 'runPrimo_gui.py'. The basic usage is to specify a xbif or conf as an optional first argument. The gui will load the specified model and start the webserver. If no model was specified, it will start with an empty Bayesian Network.

##Additinal parameters:

* --port: Specifies the port to run at (Default: 8080)
* --debug: Starts the webserver in debug mode (Default: Disabled)
* --browser/--no-browser: Enables/disables starting a webbrowser directly. (Default: Enabled)


##Controls:

###Drawing a Graph

####Create and change a (new) node

* DOUBLE-CLICK on the chart creates a new node
    * Change the Name:
        * RIGHT-CLICK on the node [--> Context Menu]
        * Select 'Show Properties' [--> Property Table]
        * DOUBLE-CLICK on the current Node Name (Title) [--> Pop-up]
        * Enter new name and 'Submit'
    * Change the CPT:
        * RIGHT-CLICK on the node [--> Context Menu]
        * Select 'Show Properties' [--> Property Table]
        * DOUBLE-CLICK on the Values (column-wise manipulable) [--> Pop-up]
        * Enter new values and 'Submit'
    * Change the Values / Value-Names:
        * RIGHT-CLICK on the node [--> Context Menu]
        * Select 'Show Properties' [--> Property Table]
        * DOUBLE-CLICK on the current Value-Names  [--> Pop-up]
            * Enter new names and / or
            * 'Add' or 'Delete' values and / or
                * This will invalidate the CPT of the target node (sets every entry to 0)
        * 'Submit' your changes
    * Change Position: 
        * DRAG and DROP the Node on the chart
    * Foreground the node:
        * DRAG or CLICK the Node to place it in the Foreground of the Nodes
        
####Create a new Edge
        
* SHIFT + DRAG from one node 1 to another node 2 and DROP 
    * This will invalidate the CPT of the target node (sets every entry to 0)
* Change the direction by starting from node 2
* In 'DBN' - 'TBN' mode (confer Section: 'DBN Navigator'):
    * SHIFT + DRAG from one node 1 to another node 2 and DROP [--> Context Menu]
    * Select Edge Type 'Dynamic' or 'Static'
    * Info: If a Dynamic Transition exists and additionally one more Edge, 
    then this Node is marked by the Attribute: 'Edges ?' ('orange' text) 

####Delete Nodes / Edges

* Selection: (Color of Node-perimeter / Edge changes into 'orange')
    * Single Node or Edge: LEFT-CLICK on the Node or the Edge 
    * Multiple Elements: CTRL + LEFT-CLICK on Nodes and / or Edges
* Delete selected Elements:
    * DELETE or BACKSPACE
* Info: Deleting a Node /Edge will correctly adapt the CPTs of all it's children but will set all entries to '0'
    
###Moving and Zooming the Graph

* Moving:
    * DRAG and DROP the Chart (DRAG a 'blank' zone not an element)      
* Zooming in and out:
    * Use the MOUSE-WHEEL for zooming and focusing
* Info: 
    * If you've lost the focus use 'Default View' in the 'Navigation Bar'. 
    
###Navigation Bar
####New

* Create a new Graph:
    * CLICK on 'NEW' [--> Pop-up]
    * Verify to create a New Graph while deleting the current one
    
####Load

* Load an existing Graph 
    * CLICK on 'Load' [--> Pop-up]
    * Enter the desired absolute Path
        * Info: The initially provided Path may vary
    * CLICK on 'Browse' to find the desired file [--> Pop-up]
        1. '.xbif' files for Discrete Baysian Networks
        2. '.conf' files for Dynamic Baysian Networks
        * Info: Take care of equal Path-Locations of the 'Browse-Menu' and the entered one 
    * Click in 'Load' if you are sure to load the new graph while deleting the current one

####Save

* Save an existing Graph 
    * CLICK on 'Save' [--> Pop-up]
    * Enter the desired absolute Path
        * Info: The initially provided Path may vary
    * Enter the File-Name in the next Input-Field
        * The Type of File is choosen automatically 
            * Discrete Baysian Networks:
                1. '{name}.xbif'
            * Dynamic Baysian Networks:
                1. '{name}.conf'
                2. '{name}-b0.xbif' 
                3. '{name}-2tbn.xbif'
        * Info: Take care of the choosen name, existing files may be lost / overwritten
        
####Default View

* CLICK on 'Default View' to reset the zoomed and moved Graph to default position
    * The (relative) Positions of the Elements don't change

####Inference Method

* Select an 'Inference Method':
    * Go over 'Inference Method' to select a Method from the Drop-Down List
    * A repeated CLICK on the (same) Method renews the calculation
        * 'Inference Methods' in Discrete Baysian Network:
            1. 'Variable Elimination'
            2. 'Factor Tree'
            3. 'Gibbs Sampling'
            4. 'Metropolis Hasting'
        * 'Inference Methods' in Dynamic Baysian Network:
            1. 'Prior Feedback'
            2. 'Soft Evidence'
* Info: No Method is choosen as default. Instead, an uniform distribution is used 
which also may arise if an error is detected. However, try to avoid errors in order to prevent a 'crash'.

####Layout Method

* Select a predefined Graph Layout:
    * Go over 'Layout Method' to select one of the following Methods from the Drop-Down List
        1. 'Deep': Deep parent-child connections are preferred
        2. 'Wide': Flat parent-child connections are preferred
        3. 'Read': If possible the Predefined (loaded file) positions are 'Read'
            * Info: If you change a Node's Position or Manipulate the Graph, then these New Positions are stored 
            and you can't read the Initial Positions. In that case you may load the Graph again using 'Load'.
    * Info: Sometimes the edges are on top of each other, in that case you have to move the nodes by hand.

####Round Digits

The CPT values are checked in order to have a Sum = 1. 
* The precision of considered decimals can be selected via the 'Round Digits' - Drop-Down List 
* Info: 
    * As a result, if a column in the CPT doesn't sum up to 1 it is either marked in the CPT itself ('orange' color)
or the Node get the Attribute: 'Not valid!' ('orange' text) which is also consistent with PRIMO's 'valid' attribute

####Graph Mode: 'Baysian Network' (BN) or 'Dynamic Baysian Network' (DBN)

* Go over 'Graph Mode' and select either 'Baysian Network' (BN) or 'Dynamic Baysian Network' (DBN) from the Drop-Down List
    * BN is the default mode
    * Selecting DBN creates additional navigation options (or loading a DBN Model):
        * 'DBN Navigator' 
        * 't = {integer}'
        * 'Next'

####DBN Navigator

* Go over 'DBN Navigator' and select one of the two Graphs from the Drop-Down List:
    * 'B0': The Network representing the initial distribution
        * The initial 'B0' only exists in 't = 0' by increasing the Time Steps 't > 0' 
        it only shows the copies of 'TBN' if possible 
    * 'TBN': The Two-Time-Slice Network representing the process
        * The 'green' Nodes mark the Nodes (and Transistions) from the Previous Time Step 't-1'
        * The 'blue' Nodes are the Current representatives 't'
        * TBN only exists in 't > 0'
            
####Change DBN Time Step: 't = {integer}' and 'Next'

* Change Time Step:
    * 't = {integer}': Indicates the Time Step of the DBN
        * CLICK on 't = {integer}' to change it: [--> Pop-up]
            1. Enter the Number of Time Steps the 'Inference Methods' should go and CLICK 'Proceed'
            2. CLICK 'Reset' to get the initial Time Ste 't = 0' / 't = 1'
    *'Next':
        * CLICK on 'Next' to go One Time Step

###Context Menus

* RIGHT-CLICK on Node:
    1. 'Show Properties': Creates CPT 
    2. 'Show Graph': Creates Bar Chart of Outcome results 
    3. 'Show Outcome': Creates Pie Chart of Outcome results 
    * If an Element is already shown, the buttons change to 'Hide' and anew CLICK deletes the Element
    
* RIGHT-CLICK on CPT (Property Table) or Bar Chart (Graph):
    1. 'Close': Delete this Element
    2. 'Stop Zooming': The Element is renewed in the left top corner and Not Affected by Zooming via MOUSEWHEEL
        * 'Enable Zooming' is now selectable and reverse this behavior

###CPT (Show Properties)

* Shows the CPT of the Node: Its Name, Values and Value Names as well as its Parents + Parent Value-Names
* You can DRAG this Element
* Foreground this Element:
    * DRAG or CLICK this Element to place it in the Foreground
* DOUBLE-CLICK on Node Name (Title): 
    * See 'Create and change a (new) node' - 'Change the Name'
* DOUBLE-CLICK on Values: 
    * See 'Create and change a (new) node' - 'Change the CPT'
* DOUBLE-CLICK on Value-Names: 
    * See 'Create and change a (new) node' - 'Change the Values / Value-Names'
* DOUBLE-CLICK on Parents: 
    * Change the Visible CPT Fields [--> Pop-up]
    * The different Drop-Down Lists of the Parents allow you to Choose its visible Values
    * Either One or All Values are selectable
        

###Bar Chart (Show Graph)

* Shows the Outcome Results influenced by the Inference Methods in a new Bar Chart
* A CLICK on the Colored Left Field set the (Hard) Evidence 
* You can DRAG this Element
* Foreground this Element:
    * DRAG or CLICK this Element to place it in the Foreground

###Pie Chart (Show Outcome)

* Shows the Outcome Results influenced by the Inference Methods around the Node 
* MOUSEOVER the Colored Fields shows the Value and its Name
* A CLICK on the Colored Field set the (Hard) Evidence  

###Additional Information
* Updates may take a little time depending on the size of the network and the chosen inference method. 
Most of this time-consuming updates are indicated by an darken screen.
* Please look in PRIMO for the correct use of this Dynamic Baysian Networks and Baysian Networks.
    * Especially the DBN is not checked for all possibilities, hence take care of the correct use.
 
##Running:

* When installed (e.g. using pip), the script runPrimo_gui.py will be added to the binary path, meaning that the gui can then be started by simply calling 'runPrimo_gui.py [Path_to_xbif]'
