import React, { useState, Fragment } from 'react';
import LinkRightClickMenu from './LinkRightClickMenu';
import './Link.css';

const Link = ({sourceNode, targetNode, radius, mouseMode, updateGraph, hide}) => {
  const [menu, setMenu] = useState(null);
  
  const openMenu = (e) => {
    setMenu({x: e.clientX, y: e.clientY})
  }

  const closeMenu = () => {
    setMenu(null);
  }

  const distance = Math.sqrt(Math.pow(targetNode.x - sourceNode.x, 2) + Math.pow(targetNode.y - sourceNode.y, 2)) - 2 * radius;
  const angle = Math.atan2(targetNode.y - sourceNode.y, targetNode.x - sourceNode.x) * 180 / Math.PI;



  return ( 
    <Fragment>
      <g
        className='link'
        transform={`rotate(${angle} ${sourceNode.x} ${sourceNode.y})`}
        onContextMenu = {(e) => {
          if(mouseMode === 'Drag') {
            e.preventDefault()
            openMenu(e)
          }
        }}
        opacity={hide ? '30%' : '100%'}
      >
        <defs>
        <marker id="arrow" viewBox="0 0 10 10" refX="10" refY="5"
          markerWidth="5" markerHeight="5">
        <path d="M 0 0 L 10 5 L 0 10 z" />
        </marker>
        </defs>
        <line x1={sourceNode.x + radius} y1={sourceNode.y} x2={sourceNode.x + distance + radius} y2={sourceNode.y} stroke="black" strokeWidth="2" markerEnd="url(#arrow)"/>
        <rect x={sourceNode.x + radius} y={sourceNode.y-10} width={distance} height='20' opacity='0%'/>
      </g>
      {menu ? (
        <LinkRightClickMenu
          source={sourceNode.name}
          target={targetNode.name}
          x={menu.x}
          y={menu.y}
          closeMenu={closeMenu}
          updateGraph={updateGraph}
        />
      ) : null}
    </Fragment>
  );
}
 
export default Link;