import React from 'react';
import ReactDOM from 'react-dom';
import Checkbox from '@material-ui/core/Checkbox';
import '../App.css';

const OutcomeLegend = ({id, node, x, y, updateGraph, sampleStep}) => {

  const changeEvidence = (value) => {
    var mode = '';
    if (node.evidence === value) {
      mode='remove'
    }
    fetch('/_changeEvidence', {method: 'POST', headers: {'Content-Type': 'application/json'}, body: JSON.stringify({nodeName: node.name, value: value, mode: mode})})
    .then(res => res.json()).then(data => {
      updateGraph(data[0]);
    });
  }

  return ReactDOM.createPortal(
    ( 
    <table className='outcome-list' style={{position: 'absolute', left: x + 'px', top: y + 'px'}}>
      <tbody>
      {node.outcomes.map((outcome, index) => 
        <tr key={index}>
          <td>
          <Checkbox 
            checked={(node.evidence === outcome.name) ? true : false}
            onChange={() => changeEvidence(outcome.name)}
            disableRipple
            size='small'
            style={{color: outcome.color, backgroundColor: 'transparent', padding: '0px'}}
            />
          </td>
          <td>
          {!(Array.isArray(outcome.value)) || sampleStep === null ? (
            <span className='noselect'>{outcome.name}</span>
          ) : (
            <span className='noselect' style={(outcome.value[sampleStep][0] === 1) ? {fontWeight: 'bold'} : null}>{outcome.name}</span>
          )}
          </td>
          <td>
            {!(Array.isArray(outcome.value)) || sampleStep === null ? (
              <div style={{height: '12px', width: outcome.value * 36 + 'px', backgroundColor: outcome.color}}></div>
            ) : (
              <div style={{height: '12px', width: outcome.value[sampleStep][2] * 36 + 'px', backgroundColor: outcome.color}}></div>
            )}
          </td>
          <td>
            {(Array.isArray(outcome.value)) && sampleStep !== null ? (
              <span className='noselect'>{outcome.value[sampleStep][1]}/{sampleStep+1}</span>
            ) : (
              <span className='noselect'>{Math.round((outcome.value + Number.EPSILON) * 1000) / 1000}</span>
          )}
          </td>
        </tr>
      )}
      </tbody>
    </table>
    ), document.getElementById('app')
  )
}
 
export default OutcomeLegend;