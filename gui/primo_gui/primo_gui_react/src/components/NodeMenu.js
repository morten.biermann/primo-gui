import React, {Fragment, useState} from 'react';
import ReactDOM from 'react-dom';
import './NodeMenu.css';
import EditIcon from '@material-ui/icons/Edit';
import CloseIcon from '@material-ui/icons/Close';
import CheckIcon from '@material-ui/icons/Check';
import RemoveIcon from '@material-ui/icons/Remove';
import MinimizeIcon from '@material-ui/icons/Minimize';
import AddIcon from '@material-ui/icons/Add';
import ErrorIcon from '@material-ui/icons/Error';

const NodeMenu = ({node, x,y, parents, changeNodeName, updateGraph, toggleShowInfo}) => {
  const [isEditingOutcomeNames, setIsEditingOutcomeNames] = useState(false);
  const [isEditingName, setIsEditingName] = useState(false);
  const [name, setName] = useState(node.name);
  const [newOutcomes, setNewOutcomes] = useState([...node.outcomes])
  const [newTableCol, setNewTableCol] = useState(null)
  const [fixed, setFixed] = useState(true);
  const [menuX, setMenuX] = useState(x);
  const [menuY, setMenuY] = useState(y);
  const [isDragging, setIsDragging] = useState(false);
  const [initiateDragging, setInitiateDragging] = useState(false);
  const [draggingOrigin, setDraggingOrigin] = useState(null);
  const [editingOutcomeColumn, setEditingOutcomeColumn] = useState(null);
  const [sumNotOneAlert, setSumNotOneAlert] = useState(false);


  const saveNameChanges = () => {
    if (name !== node.name) {
      changeNodeName(name, node.name)
      setName(node.name);
    }
    setIsEditingName(false);
  }

  const deleteOutcome = () => {
    if (newOutcomes.length > 2) {
      setNewOutcomes(newOutcomes.slice(0,-1))
    }
  }
  
  const addOutcome = () => {  
    const colors = ['#008000', '#FF0000', '#0000FF', '#FFFF00', '#00FFFF', '#FF00FF', '#800000', '#808000', '#00FF00', '#C0C0C0']
    const index = newOutcomes.length;
    var newColor = '#000000'
    if (index < colors.length) {
      newColor = colors[index];
    } else {
      newColor = Math.floor(Math.random()*16777215).toString(16);
    }
      setNewOutcomes([...newOutcomes, {name: '', color: newColor}])
  }

  const editOutcomeName = (e, id) => {
    var outcomesChanged = [...newOutcomes];
    outcomesChanged[id].name = e.target.value;
    setNewOutcomes(outcomesChanged);
  }

  const saveOutcomeChanges = () => {
    if(newOutcomes.filter(outcome => outcome.name === '').length === 0) {
      fetch('/_changeOutcomeValueNames', {method: 'POST', headers: {'Content-Type': 'application/json'}, body: JSON.stringify(
        {
          dif: newOutcomes.length - node.outcomes.length,
          newValueNames: newOutcomes.map(outcome => outcome.name),
          newValueColors: newOutcomes.map(outcome => outcome.color),
          nodeName: node.name
        })})
      .then(res => res.json()).then(data => {
        updateGraph(data[0]);
        setIsEditingOutcomeNames(false);
      });
    } else {
    }
  }

  const saveOutcomeColumnChanges = (col) => {
    const sum = newTableCol.reduce((a, b) => parseFloat(a) + parseFloat(b), 0);
    if (sum === 1) {
      fetch('/_changeOutcomeValues', {method: 'POST', headers: {'Content-Type': 'application/json'}, body: JSON.stringify(
        {
          newValues: newTableCol,
          nodeName: node.name,
          columnNr: col,
          chooseList: Array(node.parents_name.length).fill([]),
          RoundDigits: 4,
        })})
      .then(res => res.json()).then(data => {
        updateGraph(data[0]);
        setEditingOutcomeColumn(null);
        setNewTableCol(null);
        setSumNotOneAlert(false);
      });
    } else {
      setSumNotOneAlert(true);
    }
  }

  const discardOutcomeChanges = () => {
    setIsEditingOutcomeNames(false);
    setNewOutcomes(node.outcomes);
  }
  
  const discardOutcomeColumnChanges = () => {
    setEditingOutcomeColumn(null);
    setNewTableCol(getTableCol(false));
    setSumNotOneAlert(false);
  }

  const discardNameChanges = () => {
    setName(node.name);
    setIsEditingName(false);
  }

  const countParentDimensions = () => {
    var parentDimensions = [];
    parents.forEach(parent => parentDimensions = [...parentDimensions, parent.outcomes.length]);
    return parentDimensions;
  }

  const changeOutcomeColor = (e, id) => {
    var outcomesChanged = [...newOutcomes];
    outcomesChanged[id].color = e.target.value;
    setNewOutcomes(outcomesChanged);
  }

  const getTableEntry = (index, col) => {
    var reduceTable = node.table[index];
    var reduceCombinationCount = getCombinationCount();
    countParentDimensions().forEach(dim => {
      reduceCombinationCount = reduceCombinationCount / dim;
      let chooseDim = Math.floor(col / reduceCombinationCount);
      col = col % reduceCombinationCount;
      reduceTable = reduceTable[chooseDim]
    })
    return reduceTable;
  }

  const getTableCol = (col) => {
    var tableCol = [];
    for (let i = 0; i < node.outcomes.length; i++) {
      tableCol = [...tableCol, getTableEntry(i, col)]
    }
    return tableCol;
  }

  const initializeTableColEdit = (col) => {
    setEditingOutcomeColumn(col);
    setNewTableCol(getTableCol(col));
  }

  const changeTableColEntry = (e, row) => {
    const lastChar = e.target.value.slice(-1);
    if (['0','1','2','3','4','5','6','7','8','9','.'].includes(lastChar) || e.target.value === '') {
      var editTableCol = [...newTableCol];
      editTableCol[row] = e.target.value;
      setNewTableCol(editTableCol);
    }
  }

  const getCombinationCount = () => {
    return countParentDimensions().reduce( (a,b) => a * b, 1)
  }
  
  var nextSpan = 1;

  return ReactDOM.createPortal(
    (
      <Fragment>
      {isDragging ? (
        <div className='overlay'
          onMouseUp = {(e) => {
            setIsDragging(false);
          }}
          onMouseMove = {(e) => {
            setMenuX(menuX + e.clientX - draggingOrigin.x);
            setMenuY(menuY + e.clientY - draggingOrigin.y);
            setDraggingOrigin({x: e.clientX, y: e.clientY});
          }}
        />
      ) : null}
      <table className='probability-table' style={fixed ? {position: 'absolute', left: x + 'px', top: y + 'px'} : {position: 'absolute', left: menuX + 'px', top: menuY + 'px'}}>
        <thead
          onMouseDown = {(e) => {
            e.stopPropagation();
            setInitiateDragging(true);
            if(fixed) {
              setMenuX(x);
              setMenuY(y);
            }
            setFixed(false);
            setDraggingOrigin({x: e.clientX, y: e.clientY})
          }}
          onMouseMove = {(e) => {
            if (initiateDragging) {
              setInitiateDragging(false);
              setIsDragging(true);
            }
          }}
          onMouseUp = {(e) => {
            if (initiateDragging) {
              setInitiateDragging(false);
            }
          }}
        >
          <tr>
            <td colSpan='100%'>
              <div style={{display: 'flex'}}>
                {!isEditingName ? (
                  <span style={{alignSelf: 'center'}} onClick={() => setIsEditingName(true)}>{node.name}</span>
                ) : (
                  <input style={{alignSelf: 'center'}} type="text" value={name} onChange={(e) => setName(e.target.value)} className='edit-name'/>
                )}
                {isEditingName ? (
                  <Fragment>
                    <CheckIcon fontSize='small' className='check' onClick={saveNameChanges}/>
                    <CloseIcon fontSize='small' className='discard' onClick={discardNameChanges}/>
                  </Fragment>
                ) : (
                  null
                )}
                <MinimizeIcon fontSize='small' className='icon-pull-right' onClick={toggleShowInfo}/>
              </div>
            </td>
          </tr>
        </thead>
        {sumNotOneAlert ? (
        <tr>
          <td colSpan='100%' style={{color: 'red', border: '1px solid red'}}>
            <div style={{display: 'flex'}}>
              <ErrorIcon fontSize='small'/>
              <span style={{alignSelf: 'center'}}>Column values need sum 1</span>
            </div>
          </td>
        </tr>
        ) : null}
        <tbody>
          {parents.map((parent, index) => {
              const thisSpan = nextSpan;
              nextSpan = parent.outcomes.length * thisSpan;
              const loop = [];
              for(var i = 1; i <= thisSpan; i++) {
                loop.push(i);
              }
              return (
                <tr key={index}>
                  <th>{parent.name}</th>
                  {loop.map((i) =>
                    <Fragment key={i}>
                      {parent.outcomes.map((outcome, j) =>
                        <td key={i*j} colSpan={getCombinationCount()/nextSpan} className='center-text' style={{backgroundColor: outcome.color}}>{outcome.name}</td>
                      )}
                    </Fragment>
                  )}
                </tr>
              )
            }
          )}
          {newOutcomes.map((outcome, index) => {
            var table = node.table[index];
            return (
              <tr key={index}>
                <th style={isEditingOutcomeNames ? null : {backgroundColor: outcome.color}} onClick={() => setIsEditingOutcomeNames(true)}>
                  {isEditingOutcomeNames ? (
                    <Fragment>
                      <input type="text" value={outcome.name} className='edit-name' onChange={(e) => editOutcomeName(e, index)}/>
                      <input type='color' value={outcome.color} className='color-picker' onChange={(e) => changeOutcomeColor(e, index)}/>
                    </Fragment>
                  ) : <span>{outcome.name}</span>}
                </th>
                {node.parents_name.length > 0 && !(index >= node.table.length) ? (
                  <Fragment>
                    {table.flat(node.parents_name.length).map((prob, col) =>
                    <Fragment key={col}>
                      {(editingOutcomeColumn === col) ? (
                        <input type="text" value={newTableCol[index]} className='edit-name' onChange={(e) => changeTableColEntry(e, index)}/>
                      ) : (
                        <td className='center-text' onClick={() => initializeTableColEdit(col)}>{prob}</td>
                      )}
                      </Fragment>
                    )}
                  </Fragment>
                ) : (
                  <Fragment>
                    {(editingOutcomeColumn === 0) ? (
                        <input type="text" value={newTableCol[index]} className='edit-name' onChange={(e) => changeTableColEntry(e, index)}/>
                      ) : (
                        <td className='center-text' onClick={() => initializeTableColEdit(0)}>{table}</td>
                      )}
                  </Fragment>
                )}
              </tr>
            )
          })}
          {isEditingOutcomeNames || (editingOutcomeColumn !== null) ? (
            <tr>
              <td>
              <div style={{display: 'flex'}}>
                {isEditingOutcomeNames ? (
                <Fragment>
                  <AddIcon fontSize='small' onClick={addOutcome}/>
                  <RemoveIcon fontSize='small' className='discard' onClick={deleteOutcome}/>
                  <CheckIcon fontSize='small' className='icon-pull-right check' onClick={saveOutcomeChanges}/>
                  <CloseIcon fontSize='small' className='discard' onClick={discardOutcomeChanges}/>
                </Fragment>
                ) : null}
              </div>
              </td>
              {[...Array(getCombinationCount()).keys()].map((x) => 
                <td key={x}>
                <div style={{display: 'flex'}}>
                {(editingOutcomeColumn === x) ? (
                <Fragment>
                  <CheckIcon fontSize='small' className='check' onClick={() => saveOutcomeColumnChanges(x)}/>
                  <CloseIcon fontSize='small' className='discard' onClick={discardOutcomeColumnChanges}/>
                </Fragment>
                ) : null}
                </div>
                </td>
              )}
            </tr>
          ) : null}
        </tbody>
      </table>
      </Fragment>
    ), document.getElementById('app')
  )
}
 
export default NodeMenu;