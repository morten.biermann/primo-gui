import React from 'react';
import ReactDOM from 'react-dom';
import { Menu, ListItemText, MenuItem } from '@material-ui/core';

const ListItemStyle = {
  paddingTop: 0,
  paddingBottom: 0,
}

const LinkRightClickMenu = ({source, target, x, y, closeMenu, updateGraph}) => {

  const deleteEdge = () => {
    closeMenu();
    fetch('/_removeEdge', {method: 'POST', headers: {'Content-Type': 'application/json'}, body: JSON.stringify({edgeFrom: source, edgeTo: target})})
    .then(res => res.json()).then(data => {
      updateGraph(data[0]);
    });
  }

  return ReactDOM.createPortal(
    ( 
      <Menu
        keepMounted
        open={true}
        onClose={closeMenu}
        anchorReference="anchorPosition"
        anchorPosition={{ top: y, left: x }}
      >
        <MenuItem style={ListItemStyle} button onClick={deleteEdge}>
          <ListItemText primary='Delete Link'/>
        </MenuItem>
      </Menu>
    ), document.getElementById('app')
   );
}
 
export default LinkRightClickMenu;