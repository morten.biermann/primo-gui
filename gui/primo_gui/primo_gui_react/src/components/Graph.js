import React, { useState, useEffect} from 'react';
import Link from './Link';
import Sidebar from './Sidebar';
import './Graph.css';
import NodeContainer from './NodeContainer';
import NewNode from './NewNode';
import NewLink from './NewLink';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import ErrorIcon from '@material-ui/icons/ErrorTwoTone';
import Button from '@material-ui/core/Button';
import { Grid } from '@material-ui/core';


const Graph = ({initialGraphData, fileName, newGraph}) => {
  const [graph, setGraph] = useState({nodes: [], links: []});
  const [nodeDragging, setNodeDragging] = useState({isDragging: false, node: null, origin: {x: 0, y: 0}});
  const [viewDragging, setViewDragging] = useState({isDragging: false, origin: {x: 0, y: 0}});
  const [windowSize, setWindowSize] = useState({width: window.innerWidth, height: window.innerHeight});
  const [zoom, setZoom] = useState(1);
  const [viewPosition, setViewPosition] = useState({x: -180, y: 0})
  const [radius] = useState(50);
  const [mouseMode, setMouseMode] = useState('Drag');
  const [inferenceTypes, setInferenceTypes] = useState([]);
  const [currentInferenceMethod, setCurrentInferenceMethod] = useState('');
  const [newLinkStart, setNewLinkStart] = useState(null);
  const [mousePosition, setMousePosition] = useState(null);
  const [nodeStates, setNodeStates] = useState({});
  const [sampleStep, setSampleStep] = useState(null);
  const [showCircularError, setShowCircularError] = useState(false);
  const [showCurrentSample, setShowCurrentSample] = useState(false);
  const [showCurrentMarkovBlanket, setShowCurrentMarkovBlanket] = useState(false);

  const resizeWindow = () => {
    setWindowSize({width: window.innerWidth, height: window.innerHeight});
  }

  useEffect(() => {
    window.addEventListener('resize', resizeWindow);
    return () => window.removeEventListener('resize', resizeWindow)
  }, [])

  useEffect(() => {
    updateGraph(initialGraphData)
    if (initialGraphData.nodes.length > 0 ) {
      const positionsXmin = Math.min(...initialGraphData.nodes.map((node) => node.x));
      const positionsYmin = Math.min(...initialGraphData.nodes.map((node) => node.y));
      setViewPosition({x: positionsXmin - 180 - 2*radius, y: positionsYmin - 2*radius})
    }
  }, [initialGraphData]);

  const updateGraph = (graphData) => {
    setMouseMode('Drag');
    var newElements = {}
    graphData.nodes.forEach((node) => {
      if (!Object.keys(nodeStates).includes(node.name)) {
        newElements[node.name] = {hide: false, showOutcomes: true, showLegend: true, showInfo: false};
      }
    })
    setNodeStates({...nodeStates, ...newElements})
    setGraph({nodes: graphData.nodes, links: graphData.links});
    setInferenceTypes(graphData.inference_types);
    if(currentInferenceMethod !== graphData.current_inference_method) {
      if (graphData.current_inference_method === "Visualise Samples") {
        setSampleStep(0);
      } else {
        setSampleStep(null);
      }
      setCurrentInferenceMethod(graphData.current_inference_method);
    }
  }

  const addNode = (x, y) => {
    fetch('/_addNode', {method: 'POST', headers: {'Content-Type': 'application/json'}, body: JSON.stringify({'x': x, 'y': y})})
    .then(res => res.json()).then(data => {
      updateGraph(data[0]);
    });
  }

  const addLink = (startNode, endNode) => {
    fetch('/_addEdge', {method: 'POST', headers: {'Content-Type': 'application/json'}, body: JSON.stringify({'from': startNode, 'to': endNode, 'edgeType': ''})})
    .then(res => res.json()).then(data => {
      updateGraph(data[0]);
      stopDrawingLink();
      if (data[1] === 'Circular') {
        setShowCircularError(true);
      }
    });
  }

  const changeNodeName = (newName, oldName) => {
    if (graph.nodes.filter((node) => node.name === newName).length > 0) {

    } else {
      fetch('/_changeName', {method: 'POST', headers: {'Content-Type': 'application/json'}, body: JSON.stringify({newName: newName, nodeName: oldName})})
      .then(res => res.json()).then(data => {
        updateGraph(data[0]);
      });
    }
  }

  const activateNodeDragging = (id, e) => {
    setNodeDragging({isDragging: true, node: id, origin: {x: e.clientX, y: e.clientY}});
  }

  const disableNodeDragging = (id) => {
    if (nodeDragging.isDragging) {
      updateNodePosition(nodeDragging.node);
      setNodeDragging({isDragging: false, node: null});
    }
  }

  const updateNodePosition = (id) => {
    const node = graph.nodes[id];
    fetch('/_updateNodePosition', {method: 'POST', headers: {'Content-Type': 'application/json'}, body: JSON.stringify({'x': node.x, 'y': node.y, 'nodeName': node.name})})
  }

  const setNodePosition = (e) => {
    const newNodesArray = graph.nodes;
    var currentNode = newNodesArray[nodeDragging.node];
    currentNode.x += (e.clientX - nodeDragging.origin.x) * zoom;
    currentNode.y += (e.clientY - nodeDragging.origin.y) * zoom;
    newNodesArray[nodeDragging.node] = currentNode
    setGraph({nodes: newNodesArray, links: graph.links})
    setNodeDragging({...nodeDragging, origin: {x: e.clientX, y: e.clientY}})
  }

  const zoomView = (e) => {
    const newZoom = zoom * (1 + e.deltaY/1000);
    const zoomChange = zoom * e.deltaY/1000
    setZoom(newZoom)
    setViewPosition({x: viewPosition.x - (e.clientX * zoomChange), y: viewPosition.y - (e.clientY * zoomChange)})
  }

  const dragView = (e) => {
    setViewPosition({x: viewPosition.x + (viewDragging.origin.x - e.clientX) * zoom, y: viewPosition.y + (viewDragging.origin.y - e.clientY) * zoom})
    setViewDragging({...viewDragging, origin: {x: e.clientX, y: e.clientY}});
  }

  const getParents = (parents_names) => {
    var parents = [];
    parents_names.forEach((parent) => {
      parents = [...parents, graph.nodes.find((node) => node.name === parent)];
    })
    return parents;
  }

  const stopDrawingLink = () => {
    setNewLinkStart(null);
  }

  const setNewLinkEnd = (newLinkEnd) => {
    if (newLinkEnd !== newLinkStart && newLinkStart !== null) {
      addLink(graph.nodes[newLinkStart].name, graph.nodes[newLinkEnd].name);
    } else {
      stopDrawingLink();
    }
  }

  const updateNodeState = (nodeName, newState) => {
    setNodeStates({...nodeStates, [nodeName]: newState})
  }

  const getMarkovBlanket = (nodeName) => {
    fetch('/_getMarkovBlanket', {method: 'POST', headers: {'Content-Type': 'application/json'}, body: JSON.stringify({nodeName: nodeName})})
      .then(res => res.json()).then(data => {
        const markovBlanket = data[0];
        var newElements = {}
        graph.nodes.forEach((node) => {
          const inMarkovBlanket = markovBlanket.includes(node.name);
          if (inMarkovBlanket) {
            newElements[node.name] = {...nodeStates[node.name], hide: false};
          } else {
            newElements[node.name] = {...nodeStates[node.name], hide: true};
          }
        })
        setNodeStates({...nodeStates, ...newElements})
      });
  }

  const updateAllStates = (state, value) => {
    var newNodeStates = {...nodeStates};
    for (let key in nodeStates) {
      newNodeStates[key][state] = value;
    }
    setNodeStates(newNodeStates);
  }

  const toggleShowCurrentSample = () => {
    setShowCurrentSample(!showCurrentSample);
  }

  const toggleShowCurrentMarkovBlanket = () => {
    setShowCurrentMarkovBlanket(!showCurrentMarkovBlanket)
  }

  useEffect(() => {
    if (showCurrentMarkovBlanket && sampleStep) {
      graph.nodes.forEach((node) => {
        if (node.outcomes[0].value[sampleStep][3]) {
          getMarkovBlanket(node.name);
          return;
        }
      })
    }
  }, [showCurrentMarkovBlanket, sampleStep])

  return ( 
    <div id='app'>
      <Dialog
        open={showCircularError}
        onClose={() => setShowCircularError(false)}
      >
        <DialogContent>
          <Grid container direction="row" alignItems="center">
            <Grid item>
              <ErrorIcon style={{color: 'red'}}/>
            </Grid>
            <Grid item>
              Bayesian Networks cannot be circular
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button onClick= {() => setShowCircularError(false)}>
            Close
          </Button>
        </DialogActions>
      </Dialog>
      <Sidebar
        mouseMode={mouseMode}
        updateGraph={updateGraph}
        inferenceTypes={inferenceTypes}
        currentInferenceMethod={currentInferenceMethod}
        setMouseMode={setMouseMode}
        updateAllStates={updateAllStates}
        newGraph={newGraph}
        fileName={fileName}
        sampleStep={sampleStep}
        setSampleStep={setSampleStep}
        showCurrentSample={showCurrentSample}
        toggleShowCurrentSample={toggleShowCurrentSample}
        showCurrentMarkovBlanket={showCurrentMarkovBlanket}
        toggleShowCurrentMarkovBlanket={toggleShowCurrentMarkovBlanket}
      />
      <div id='graph' onWheel={zoomView}>
        <svg width={windowSize.width} height={windowSize.height}
          viewBox={`${viewPosition.x} ${viewPosition.y} ${windowSize.width * zoom} ${windowSize.height * zoom}`}
          className='background-svg'
        
          onMouseDown = {(e) => {
            if(mouseMode === 'Drag') {
              setViewDragging({isDragging: true, origin: {x: e.clientX, y: e.clientY}});
            } else if (mouseMode ==='Node') {
              addNode(e.clientX*zoom + viewPosition.x, e.clientY*zoom + viewPosition.y)
            }
          }}
          onMouseMove = {(e) => {
            if(mouseMode === 'Drag') {
              if (nodeDragging.isDragging) {
                setNodePosition(e)
              } else if (viewDragging.isDragging) {
                dragView(e)
              }
            } else if(mouseMode === 'Link' || mouseMode === 'Node') {
              setMousePosition({x: e.clientX, y: e.clientY})
            }
          }}
          
          onMouseUp = {() => {
            if (mouseMode === 'Drag') {
              if (nodeDragging.isDragging) {
                disableNodeDragging();
              } else if (viewDragging.isDragging) {
                setViewDragging({isDragging:false, origin: {x: 0, y: 0}})
              }
            } else if (mouseMode === 'Link') {
              stopDrawingLink();
            }
          }}

          onMouseLeave = {() => {
            if (mouseMode === 'Drag') {
              if (nodeDragging.isDragging) {
                disableNodeDragging();
              } else if (viewDragging.isDragging) {
                setViewDragging({isDragging:false, origin: {x: 0, y: 0}})
              }
            } else if (mouseMode === 'Link') {
              stopDrawingLink();
            }
          }}
        >
          {graph.links.map((link, index) => 
            <Link key={index} sourceNode={graph.nodes[link.source]} targetNode={graph.nodes[link.target]} radius={radius} mouseMode={mouseMode} updateGraph={updateGraph} hide={nodeStates[graph.nodes[link.source].name].hide || nodeStates[graph.nodes[link.target].name].hide}/>
          )}
          {graph.nodes.map((node, index) => 
            <NodeContainer
              key={node.name}
              id={index}
              node={node}
              radius={radius}
              activateNodeDragging={activateNodeDragging}
              mouseMode={mouseMode}
              viewPosition={viewPosition}
              zoom={zoom}
              getParents={getParents}
              updateGraph={updateGraph}
              changeNodeName={changeNodeName}
              setNewLinkStart={setNewLinkStart}
              setNewLinkEnd={setNewLinkEnd}
              nodeState={nodeStates[node.name]}
              updateNodeState={updateNodeState}
              getMarkovBlanket={getMarkovBlanket}
              sampleStep={sampleStep}
              showCurrentSample={showCurrentSample}
            />
          )}
          {(newLinkStart !== null) ? (
            <NewLink sourceX={graph.nodes[newLinkStart].x} sourceY={graph.nodes[newLinkStart].y} targetX={(mousePosition.x)*zoom + viewPosition.x - 1} targetY={(mousePosition.y)*zoom + viewPosition.y - 1}/>
          ) : null}
          {(mouseMode === 'Node' && mousePosition !== null) ? (
            <NewNode x={mousePosition.x*zoom + viewPosition.x} y={mousePosition.y*zoom + viewPosition.y} radius={radius}/>
          ) : null}
        </svg>
      </div>
    </div>
   );
  
}
 
export default Graph;