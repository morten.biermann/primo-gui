import React, { Fragment } from 'react';
import '../App.css';

const Node = ({id, node, radius, activateNodeDragging, mouseMode, openMenu, showOutcomes, setNewLinkStart, setNewLinkEnd, hide, currentSample}) => {
  const circumference = Math.PI * radius;
  
  var nextRotation = -90;

  return (
    <g className='node'
      onMouseDown = {(e) => {
        e.stopPropagation()
        if(mouseMode === 'Drag') {
          activateNodeDragging(id, e);
        } else if(mouseMode === 'Link') {
          setNewLinkStart(id);
        }
      }}

      onMouseUp = {(e) => {
        if(mouseMode === 'Link') {
          e.stopPropagation();
          setNewLinkEnd(id);
        }
      }}

      onContextMenu = {(e) => {
        if(mouseMode === 'Drag') {
          e.preventDefault()
          openMenu(e)
        }
      }}

      opacity={hide ? '40%' : '100%'}
      >
      <circle cx={node.x} cy={node.y} r={radius} stroke='black' strokeWidth={!currentSample ? '2' : '8'} fill='orange'/>
      {showOutcomes ? (
        <Fragment>
          {node.outcomes.map((outcome) => {
            const rotation = nextRotation;
            nextRotation = rotation + 360*outcome.value;
            return (
              <circle key={outcome.name} cx={node.x} cy={node.y} r={radius/2} fill='transparent'
              stroke={outcome.color} strokeWidth={radius} strokeDasharray={`${outcome.value * circumference} ${circumference}`}
              transform={`rotate(${rotation} ${node.x} ${node.y})`}/>
            )
            })}
        </Fragment>
      ) : null }
      {node.evidence ? (
        <Fragment>
          <circle cx={node.x} cy={node.y} r={radius} stroke='black' stroke-width='2' fill='gray' opacity='0.6'/>
          <line x1={node.x - radius} y1={node.y} x2={node.x + radius} y2={node.y} stroke="gray" stroke-width="3" opacity='0.8' transform={`rotate(45 ${node.x} ${node.y})`}/>
          <line x1={node.x - radius} y1={node.y} x2={node.x + radius} y2={node.y} stroke="gray" stroke-width="3" opacity='0.8' transform={`rotate(-45 ${node.x} ${node.y})`}/>
          <line x1={node.x - radius} y1={node.y} x2={node.x + radius} y2={node.y} stroke="gray" stroke-width="3" opacity='0.8' transform={`rotate(90 ${node.x} ${node.y})`}/>
          <line x1={node.x - radius} y1={node.y} x2={node.x + radius} y2={node.y} stroke="gray" stroke-width="3" opacity='0.8' />
        </Fragment>
      ) : null }
      <text x={node.x} y={node.y} dominantBaseline="middle" textAnchor='middle' fill='black' className='noselect' fontWeight='bold'>{node.name}</text>
    </g>
  );
}
 
export default Node;