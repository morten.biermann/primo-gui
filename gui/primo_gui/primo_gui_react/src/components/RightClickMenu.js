import React from 'react';
import ReactDOM from 'react-dom';
import { ListItemText, ListItemSecondaryAction, Checkbox, Menu, MenuItem } from '@material-ui/core';


const ListItemStyle = {
  paddingTop: 0,
  paddingBottom: 0,
}

const RightClickMenu = ({id, x, y, showOutcomes, toggleShowOutcomes, showInfo, toggleShowInfo, showLegend, toggleShowLegend, hide, toggleHide, closeMenu, updateGraph, nodeName, showMarkovBlanket}) => {

  const deleteNode = () => {
    fetch('/_removeNode', {method: 'POST', headers: {'Content-Type': 'application/json'}, body: JSON.stringify({nodeName: nodeName})})
    .then(res => res.json()).then(data => {
      updateGraph(data[0]);
    });
  }

  return ReactDOM.createPortal(
    ( 
      <Menu
        keepMounted
        open={true}
        onClose={closeMenu}
        anchorReference="anchorPosition"
        anchorPosition={{ top: y, left: x }}
      >
        <MenuItem style={ListItemStyle} button onClick={toggleShowOutcomes}>
          <ListItemText primary='Show Outcomes' />
          <ListItemSecondaryAction>
          <Checkbox 
              checked={showOutcomes ? true : false}
              onChange={toggleShowOutcomes}
              disableRipple
              size='small'
              style={{backgroundColor: 'transparent', padding: '0px'}}
              />
          </ListItemSecondaryAction>
        </MenuItem>
        <MenuItem style={ListItemStyle} button onClick={toggleShowLegend}>
          <ListItemText primary='Show Legend' />
          <ListItemSecondaryAction>
          <Checkbox 
              checked={showLegend ? true : false}
              onChange={toggleShowLegend}
              disableRipple
              size='small'
              style={{backgroundColor: 'transparent', padding: '0px'}}
              />
          </ListItemSecondaryAction>
        </MenuItem>
        <MenuItem style={ListItemStyle} button onClick={toggleHide}>
          <ListItemText primary='Hide Node' />
          <ListItemSecondaryAction>
          <Checkbox 
              checked={hide ? true : false}
              onChange={toggleHide}
              disableRipple
              size='small'
              style={{backgroundColor: 'transparent', padding: '0px'}}
              />
          </ListItemSecondaryAction>
        </MenuItem>
        <MenuItem style={ListItemStyle} button onClick={toggleShowInfo}>
          <ListItemText primary='Show Info' />
          <ListItemSecondaryAction>
          <Checkbox 
              checked={showInfo ? true : false}
              onChange={toggleShowInfo}
              disableRipple
              size='small'
              style={{backgroundColor: 'transparent', padding: '0px'}}
              />
          </ListItemSecondaryAction>
        </MenuItem>
        <MenuItem style={ListItemStyle} button onClick={showMarkovBlanket}>
          <ListItemText primary='Show Markov Blanket' />
        </MenuItem>
        <MenuItem style={ListItemStyle} button onClick={deleteNode}>
          <ListItemText primary='Delete Node'/>
        </MenuItem>
      </Menu>
    ), document.getElementById('app')
   );
}
 
export default RightClickMenu;