import React from 'react';

const NewNode = ({x, y, radius}) => {

  return (
    <g className='node'>
      <circle cx={x} cy={y} r={radius} stroke='black' stroke-width='2' fill='orange' opacity='50%'/>
    </g>
  );
}
 
export default NewNode;