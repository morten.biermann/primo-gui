import React, { useState, Fragment } from 'react';
import {
  Drawer,
  Divider,
  List,
  ListItem,
  ListItemText,
  ListSubheader,
  Menu,
  MenuItem,
  Button,
  ButtonGroup,
  Slider, ListItemSecondaryAction, Switch, IconButton, Grid, Checkbox
} from '@material-ui/core';
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord';
import CallMadeIcon from '@material-ui/icons/CallMade';
import ControlCameraIcon from '@material-ui/icons/ControlCamera';
import './Sidebar.css';

const ListItemStyle = {
  paddingTop: 0,
  paddingBottom: 0,
}

const ButtonStyle = {
  paddingLeft: 7,
  paddingRight: 7,
}

const Sidebar = ({mouseMode, updateGraph, inferenceTypes, currentInferenceMethod, setMouseMode, updateAllStates, newGraph, fileName, sampleStep, setSampleStep, showCurrentSample, toggleShowCurrentSample, showCurrentMarkovBlanket, toggleShowCurrentMarkovBlanket}) => {
  const [showViewMenu, setShowViewMenu] = useState(false);
  const [showFileMenu, setShowFileMenu] = useState(false);

  const toggleViewMenu = (e) => {
    if (showViewMenu) {
      setShowViewMenu(false);
    } else {
      setShowViewMenu(e.currentTarget)
    }
  }

  const toggleFileMenu = (e) => {
    if (showFileMenu) {
      setShowFileMenu(false);
    } else {
      setShowFileMenu(e.currentTarget)
    }
  }

  const changeFile = (e) => {
    const file = e.target.files[0];
    const reader = new FileReader();
    reader.readAsText(file, 'UTF-8')
    reader.onload = () => {
      fetch('/_loadGraph', {method: 'POST', headers: {'Content-Type': 'application/json'}, 
      body: JSON.stringify({'content': reader.result, 'path': file.name, 'width': window.innerWidth, 'height': window.innerHeight})})
      .then(res => res.json()).then(data => {
        newGraph(file.name, data[0]);
      });
    }
  }

  const loadNewGraph = (e) => {
    fetch('/_newGraph', {method: 'POST', headers: {'Content-Type': 'application/json'}, body: JSON.stringify({height: window.innerHeight, width: window.innerWidth, Radius: 50})})
    .then(res => res.json()).then(data => {
      newGraph('new-graph.xbif', data[0]);
    });  
  }
  
  const changeMethod = (inferenceMethod) => {
    fetch('/_changeMethod', {method: 'POST', headers: {'Content-Type': 'application/json'}, body: JSON.stringify({method: inferenceMethod})})
    .then(res => res.json()).then(data => {
      updateGraph(data[0]);
    });
  }

  const saveFile = () => {
    fetch('/_getGraphString', {method: 'GET', headers: {'Content-Type': 'application/json'}})
    .then(res => res.json()).then(data => {
      var FileSaver = require('file-saver');
      var blob = new Blob([data[0]]);
      FileSaver.saveAs(blob, fileName);
    });
  }

  return ( 
    <Drawer variant="permanent" anchor="left" width={180}>
      <List>
        <ListSubheader component="div">
          General
        </ListSubheader>
        <ListItem style={ListItemStyle} id='anchor' button onClick={toggleFileMenu}>
          <ListItemText primary='File' />
          <Menu
            open={Boolean(showFileMenu)}
            anchorEl={showFileMenu}
            getContentAnchorEl={null}
            anchorOrigin={{ vertical: "center", horizontal: "right" }}
            transformOrigin={{ vertical: "center", horizontal: "left" }}
            keepMounted
            onClose={() => setShowFileMenu(false)}
          >
            <ListItem style={ListItemStyle} button onClick={loadNewGraph} component='label'>
              <ListItemText primary='New Graph' />
            </ListItem>
            <ListItem style={ListItemStyle} button component='label'>
              <ListItemText primary='Load Graph' />
              <input
                type="file"
                accept='.xbif'
                style={{ display:'none'}}
                onChange={changeFile}
              />
            </ListItem>
            <ListItem style={ListItemStyle} button onClick={saveFile} component='label'>
              <ListItemText primary='Save Graph' />
            </ListItem>
          </Menu>
        </ListItem>
        <ListItem style={ListItemStyle} id='anchor' button onClick={toggleViewMenu}>
          <ListItemText primary='View' />
          <Menu
            open={Boolean(showViewMenu)}
            anchorEl={showViewMenu}
            getContentAnchorEl={null}
            anchorOrigin={{ vertical: "center", horizontal: "right" }}
            transformOrigin={{ vertical: "center", horizontal: "left" }}
            keepMounted
            onClose={() => setShowViewMenu(false)}
          >
            <MenuItem>
              <ListItemText>Nodes</ListItemText>
              <Button color="primary" onClick={() => updateAllStates('hide', false)}>Show All</Button>
              <Button color="secondary" onClick={() => updateAllStates('hide', true)}>Hide All</Button>
            </MenuItem>
            <MenuItem>
              <ListItemText>Outcomes</ListItemText>
              <Button color="primary" onClick={() => updateAllStates('showOutcomes', true)}>Show All</Button>
              <Button color="secondary" onClick={() => updateAllStates('showOutcomes', false)}>Hide All</Button>
            </MenuItem>
            <MenuItem>
              <ListItemText>Legend</ListItemText>
              <Button color="primary" onClick={() => updateAllStates('showLegend', true)}>Show All</Button>
              <Button color="secondary" onClick={() => updateAllStates('showLegend', false)}>Hide All</Button>
            </MenuItem>
            <MenuItem>
              <ListItemText>Info</ListItemText>
              <Button color="primary" onClick={() => updateAllStates('showInfo', true)}>Show All</Button>
              <Button color="secondary" onClick={() => updateAllStates('showInfo', false)}>Hide All</Button>
            </MenuItem>
          </Menu>
        </ListItem>
        <Divider/>
        <ListSubheader>
          Edit
        </ListSubheader>
        <ListItem style={{padding: '0px'}}>
          <ButtonGroup size='small' disableElevation>
            <Button onClick={() => setMouseMode('Drag')} style={(mouseMode === 'Drag') ? {backgroundColor: '#d6d6d6', ...ButtonStyle} : ButtonStyle}>
              <Grid
                container
                direction="column"
                justify="center"
                alignItems="center"
              >
                <ControlCameraIcon/>
                <span style={{fontSize: '10px'}}>Default</span>
              </Grid>
            </Button>
            <Button disableRipple onClick={() => setMouseMode('Node')} style={(mouseMode === 'Node') ? {backgroundColor: '#d6d6d6', ...ButtonStyle} : ButtonStyle}>
              <Grid
                container
                direction="column"
                justify="center"
                alignItems="center"
              >
              <FiberManualRecordIcon />
                <span style={{fontSize: '10px'}}>Add Node</span>
              </Grid>
            </Button>
            <Button onClick={() => setMouseMode('Link')} style={(mouseMode === 'Link') ? {backgroundColor: '#d6d6d6', ...ButtonStyle} : ButtonStyle}>
              <Grid
                  container
                  direction="column"
                  justify="center"
                  alignItems="center"
              >
              <CallMadeIcon/>
                <span style={{fontSize: '10px'}}>Add Edge</span>
              </Grid>
            </Button>
          </ButtonGroup>
        </ListItem>
      <Divider/>
      <ListSubheader>
        Inference
      </ListSubheader>
        <ListItem style={ListItemStyle} button onClick={() => changeMethod('')} selected={(currentInferenceMethod === '') ? true : false}>
          <ListItemText primary='None'/>
        </ListItem>
      {inferenceTypes.map((type, index) =>
        type === 'Visualise Samples' ? null : (
            <ListItem style={ListItemStyle} key={index} button onClick={() => changeMethod(type)} selected={(currentInferenceMethod === type) ? true : false}>
              <ListItemText primary={type}/>
            </ListItem>
      )
      )}
      <Divider/>
      <ListSubheader>
        Visualisation
      </ListSubheader>
      <ListItem style={ListItemStyle} button onClick={() => changeMethod('Visualise Samples')} selected={(currentInferenceMethod === 'Visualise Samples') ? true : false}>
        <ListItemText primary={'Gibbs Sampling'}/>
      </ListItem>
      {(sampleStep !== null) ? (
        <Fragment>
          <ListItem style={{padding: 0}}>
          <Slider variant="determinate" value={sampleStep+1} min={0} max={999} step={1} onChange={(e, newValue) => setSampleStep(newValue)}/>
          </ListItem>
          
          <ListItem style={{padding: 0}}>
          <ButtonGroup variant='text'>
            <Button onClick={() => setSampleStep(0)}>Start</Button>
            <Button onClick={() => setSampleStep(sampleStep - 1)} disabled={sampleStep < 1 ? true : false}>-1</Button>
            <Button onClick={() => setSampleStep(sampleStep + 1)} disabled={sampleStep > 998 ? true : false}>+1</Button>
            <Button onClick={() => setSampleStep(999)}>End</Button>
          </ButtonGroup>
          </ListItem>
          <ListItem style={ListItemStyle}>
            <ListItemText primary={'Current Sample'}/>
            <ListItemSecondaryAction>
              <Checkbox 
                checked={showCurrentSample}
                onChange={toggleShowCurrentSample}
                disableRipple
                size='small'
                style={{backgroundColor: 'transparent', padding: '0px'}}
              />
            </ListItemSecondaryAction>
          </ListItem>
          <ListItem style={ListItemStyle}>
            <ListItemText primary={'Markov Blanket'}/>
            <ListItemSecondaryAction>
              <Checkbox 
                checked={showCurrentMarkovBlanket}
                onChange={toggleShowCurrentMarkovBlanket}
                disableRipple
                size='small'
                style={{backgroundColor: 'transparent', padding: '0px'}}
              />
            </ListItemSecondaryAction>
          </ListItem>
        </Fragment>
      ) : null}
      </List>
    </Drawer>
  );
}
 
export default Sidebar;