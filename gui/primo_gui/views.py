#!/usr/bin/env python
# -*- coding: utf-8 -*-


import numpy as np
import os

from flask import render_template, request, jsonify

from primo_gui import app
import primo_gui.util as util
import primo_gui.pimoGuiGraph as nG

from primo2.nodes import DiscreteNode
from primo2.io import XMLBIFParser
from primo2.io import DBNSpec


ng = nG.GuiGraph()

@app.route('/')
def index():
    path = app.filename
    if path:
        try:
            if path[-5:] == ".xbif":
                initial_bn = XMLBIFParser.parse(path, ignoreProperties=False)
                ng.new_graph(graph_mode=["BN", ""])
                util.set_bn(ng, initial_bn)
                ng.inference_methods.set_bn(initial_bn)
            elif path[-5:] == ".conf":
                initial_dbn = DBNSpec.parse(path, ignoreProperties=False)
                ng.new_graph(graph_mode=["DBN", "B0"])
                ng.inference_methods.set_dbn(initial_dbn)
        except:
            app.logger.info(
                "File {} could not be read. Are you sure it is a correct .xbif or .conf file?".format(app.filename))

    cur_dir = os.getcwd()
    sep = os.sep
    ng.dir = cur_dir + sep
    ng.current_layout = "Read"

    return render_template("index.html")


@app.route('/_getGraph', methods=['POST'])
def get_graph():
    json_request = request.get_json()
    ng.width = json_request["width"]
    ng.height = json_request["height"]
    ng.node_radius = json_request["Radius"]

    util.set_graph(ng)
    status = "Create Graph"

    #app.logger.info(status)
    return jsonify([ng.graph, status])

@app.route('/_newGraph', methods=['POST'])
def new_graph():
    json_request = request.get_json()
    ng.width = json_request["width"]
    ng.height = json_request["height"]
    ng.node_radius = json_request["Radius"]

    ng.new_graph(graph_mode=["BN", ""])
    util.set_graph(ng)
    status = "Create Graph"

    #app.logger.info(status)
    return jsonify([ng.graph, status])

@app.route('/_addNode', methods=['POST'])
def add_node():
    json_request = request.get_json()
    x = json_request["x"]
    y = json_request["y"]

    bn = util.get_bn(ng)

    name = ('node' + str(ng.get_next_nr()))
    while bn.node_lookup.get(name, False):
        name = ('node' + str(ng.get_next_nr()))

    if ng.graph_mode[0] == "DBN" and ng.graph_mode[0] == "B0":
        tbn = ng.inference_methods.get_tbn()
        while tbn.node_lookup.get(name, False):
            name = ('node' + str(ng.get_next_nr()))
    elif ng.graph_mode[0] == "DBN" and ng.graph_mode[0] == "TBN":
        b0 = ng.inference_methods.get_b0()
        while b0.node_lookup.get(name, False):
            name = ('node' + str(ng.get_next_nr()))

    node = DiscreteNode(name, ("true", "false"))
    node.set_cpd([0.5, 0.5])
    node.meta.append("position = (" + str(x) + "," + str(y) + ")")

    bn.add_node(node)
    util.set_bn(ng, bn)
    if ng.graph_mode[0] == "DBN":
        ng.inference_methods.reset()

    util.set_graph(ng)
    status = "New Node:" + name

    app.logger.info(status)
    return jsonify([ng.graph, status])


@app.route('/_addEdge', methods=['POST'])
def add_edge():
    json_request = request.get_json()
    from_name = json_request["from"]
    to_name = json_request["to"]
    edge_type = json_request["edgeType"]

    bn = util.get_bn(ng)

    mode = util.check_new_edge(bn, ng, from_name, to_name)
    status = "error"

    if ng.graph_mode[0] == "DBN":
        transitions = ng.inference_methods.get_dbn().transitions
        if tuple([from_name, to_name]) in transitions:
            status = "Dynamic transitions still exists"
            mode = -1
        elif tuple([to_name, from_name]) in transitions:
            status = "Dynamic transitions still exists"
            mode = -1
        ng.inference_methods.reset()

    if mode == 0:
        bn.add_edge(from_name, to_name)
        util.set_bn(ng, bn)
        if ng.graph_mode[0] == "DBN" and edge_type == "DBN":
            dbn = ng.inference_methods.get_dbn()
            dbn.add_transition(from_name, to_name)
            ng.inference_methods.set_dbn(dbn)
        if ng.graph_mode[0] == "DBN":
            ng.inference_methods.reset()
        util.set_graph(ng)
        status = "New Edge: " + from_name + " -> " + to_name

    elif mode == 3:  # circular
        status = "Circular"

    elif mode == 2:  # edge has been there but wrong direction
        bn.remove_edge(to_name, from_name)
        bn.add_edge(from_name, to_name)
        util.set_bn(ng, bn)
        if ng.graph_mode[0] == "DBN" and edge_type == "DBN":
            dbn = ng.inference_methods.get_dbn()
            dbn.add_transition(from_name, to_name)
            ng.inference_methods.set_dbn(dbn)
        if ng.graph_mode[0] == "DBN":
            ng.inference_methods.reset()
        util.set_graph(ng)
        status = "Direction of edge changed. New Edge: " + from_name + " -> " + to_name

    elif mode == 1:  # edge is still existing, do nothing
        status = "Edge still exists"

    app.logger.info(status)
    return jsonify([ng.graph, status])

@app.route('/_removeNode', methods=['POST'])
def remove_node():
    json_request = request.get_json()
    node_name = json_request["nodeName"]

    bn = util.get_bn(ng)

    evidence = ng.inference_methods.get_evidence()

    bn.remove_node(node_name)
    if node_name in evidence:
        del evidence[node_name]

    ng.inference_methods.set_evidence(evidence)

    if ng.graph_mode == ["DBN", "TBN"]:
        dbn = ng.inference_methods.get_dbn()
        transitions = dbn.transitions
        del_transitions = [i for i, t in enumerate(transitions) if t[0] in node_names or t[1] in node_names]
        if del_transitions:
            del_transitions.sort(reverse=True)
            for i in del_transitions:
                del dbn.transitions[i]

    util.set_bn(ng, bn)

    if ng.graph_mode[0] == "DBN":
        ng.inference_methods.reset()

    util.set_graph(ng)
    status = "Node Removed"

    app.logger.info(status)
    return jsonify([ng.graph, status])

@app.route('/_removeEdge', methods=['POST'])
def remove_edge():
    json_request = request.get_json()
    edge_from = json_request["edgeFrom"]
    edge_to = json_request["edgeTo"]

    bn = util.get_bn(ng)

    bn.remove_edge(edge_from, edge_to)
    if ng.graph_mode == ["DBN", "TBN"]:
        dbn = ng.inference_methods.get_dbn()
        for i, transition in enumerate(dbn.transitions):
            if transition == tuple([edge_from, edge_to]):
                del dbn.transitions[i]
                break

    util.set_bn(ng, bn)

    if ng.graph_mode[0] == "DBN":
        ng.inference_methods.reset()

    util.set_graph(ng)
    status = "Edge Removed"

    app.logger.info(status)
    return jsonify([ng.graph, status])


@app.route('/_updateNodePosition', methods=['POST'])
def update_node_position():
    json_request = request.get_json()
    x = json_request["x"]
    y = json_request["y"]
    node_name = json_request["nodeName"]

    bn = util.get_bn(ng)

    if ng.is_first_position_update:
        util.set_node_position(ng.graph, node_name, x, y, bn=bn)
        util.update_bn_positions(bn, ng.graph)
        ng.is_first_position_update = False
    else:
        util.set_node_position(ng.graph, node_name, x, y, bn=bn)

    status = "Node position updated"

    app.logger.info(status)
    return jsonify(["", status])


@app.route('/_changeOutcomeValueNames', methods=['POST'])
def change_outcome_value_names():
    json_request = request.get_json()
    dif = json_request["dif"]
    new_value_names = json_request["newValueNames"]
    new_value_colors = json_request["newValueColors"]
    node_name = json_request["nodeName"]

    bn = util.get_bn(ng)

    bn_node = bn.get_node(node_name)

    util.update_colors(bn, node_name, new_value_names, new_value_colors)

    evidence = ng.inference_methods.get_evidence()
    if node_name in evidence:
        del evidence[node_name]
    ng.inference_methods.set_evidence(evidence)

    if dif == 0:
        bn_node.values = new_value_names
        status = "Outcome value names changed"
    else:
        bn_node.values = new_value_names
        # children = bn.get_children(node_name)
        len_new = len(new_value_names)

        cpd_copy = bn_node.cpd.copy()
        if dif < 0:
            cpd_copy = cpd_copy[0:len_new]
        elif dif > 0:
            for i in range(dif):
                cpd_copy = np.append(cpd_copy, [cpd_copy[0]*0], axis=0)

        bn.change_node_values(node_name, new_value_names)
        bn_node.set_cpd(cpd_copy)
        bn_node.valid = False

        # if dif < 0:
        #     bn_node.cpd = bn_node.cpd[0:len_new].copy()
        #     # bn_node.cpd[-1] += 1-np.sum(bn_node.cpd, axis=0)        # if sum should be 1
        #     for child in children:
        #         child.valid = False
        #         child._update_dimensions()
        #
        # elif dif > 0:
        #     for i in range(dif):
        #         bn_node.cpd = np.append(bn_node.cpd, [bn_node.cpd[0]*0], axis=0)
        #     for child in children:
        #         child.valid = False
        #         child._update_dimensions()
        status = "Outcome value_names changed"

    util.set_bn(ng, bn)
    if ng.graph_mode[0] == "DBN":
        ng.inference_methods.reset()

    util.set_graph(ng)

    app.logger.info(status)
    return jsonify([ng.graph, status])


@app.route('/_changeName', methods=['POST'])
def change_name():
    json_request = request.get_json()
    new_name = json_request["newName"]
    node_name = json_request["nodeName"]

    bn = util.get_bn(ng)

    evidence = ng.inference_methods.get_evidence()

    if node_name in evidence:
        evidence[new_name] = evidence[node_name]
        del evidence[node_name]

    bn.change_node_name(node_name, new_name)

    if ng.graph_mode == ["DBN", "TBN"]:
        dbn = ng.inference_methods.get_dbn()
        transitions = dbn.transitions.copy()
        del_transitions = []
        for i, t in enumerate(transitions):
            if t[0] == node_name:
                dbn.add_transition(new_name, t[1])
                del_transitions.append(i)
            elif t[1] == node_name:
                dbn.add_transition(t[0], new_name)
                del_transitions.append(i)

        if del_transitions:
            del_transitions.sort(reverse=True)
            for i in del_transitions:
                del dbn.transitions[i]

    ng.inference_methods.set_evidence(evidence)
    util.set_bn(ng, bn)
    if ng.graph_mode[0] == "DBN":
        ng.inference_methods.reset()

    util.set_graph(ng)
    status = "Name changed"

    app.logger.info(status)
    return jsonify([ng.graph, status])


@app.route('/_changeOutcomeValues', methods=['POST'])
def change_outcome_values():
    json_request = request.get_json()
    new_values = json_request["newValues"]
    node_name = json_request["nodeName"]
    column_nr = int(json_request["columnNr"])
    choose_list = json_request["chooseList"]
    ndigits = json_request["RoundDigits"]

    ng_node = util.get_node_copy(ng.graph, node_name)
    p_order = ng_node["parents_name"]

    flat_len = 1
    for i, parent in enumerate(p_order):
        if len(choose_list[i]) == 0:
            parent_outcomes = util.get_node_copy(ng.graph, parent)["outcomes"]
            flat_len *= len(parent_outcomes)
            for outcome in parent_outcomes:
                choose_list[i].append(outcome["name"])

    new_dict = dict()
    for i, values in enumerate(choose_list):
        idx = int(column_nr / (flat_len / len(values)))
        column_nr = column_nr % (flat_len / len(values))
        flat_len = flat_len / len(values)
        new_dict[p_order[i]] = values[idx]

    bn = util.get_bn(ng)

    bn_node = bn.get_node(node_name)
    for i, valueName in enumerate(bn_node.values):
        bn_node.set_probability(valueName, new_values[i], parentValues=new_dict)
    sums = np.sum(bn_node.cpd.copy(), axis=0).flatten()     # sum over its values
    node_valid = True

    if hasattr(sums, '__len__'):  # numpy array
        for i in sums:
            if round(i, ndigits=ndigits) != 1:
                node_valid = False
                break
    else:
        node_valid = False

    bn_node.valid = node_valid

    util.set_bn(ng, bn)
    if ng.graph_mode[0] == "DBN":
        ng.inference_methods.reset()

    util.set_graph(ng)
    status = "CPT changed"

    app.logger.info(status)
    return jsonify([ng.graph, status])


@app.route('/_changeMethod', methods=['POST'])
def change_method():
    json_request = request.get_json()
    inference_method = json_request["method"]

    ng.current_inference_method = inference_method

    util.set_graph(ng)
    status = "Method changed"

    app.logger.info(status)
    return jsonify([ng.graph, status])


@app.route('/_changeEvidence', methods=['POST'])
def change_evidence():
    json_request = request.get_json()
    node_name = json_request["nodeName"]
    value = json_request["value"]
    mode = json_request["mode"]
    evidence = ng.inference_methods.get_evidence()

    if mode == "remove":
        del evidence[node_name]
    else:
        evidence[node_name] = value
    
    ng.inference_methods.set_evidence(evidence)
    
    util.set_graph(ng)
    status = "Evidence changed"

    app.logger.info(status)
    return jsonify([ng.graph, status])


@app.route('/_saveGraph', methods=['POST'])
def save_graph():
    json_request = request.get_json()
    
    file_path = json_request["path"]
    file_name = json_request["fileName"]

    if file_path == "":
        cur_dir = os.getcwd()
        sep = os.sep
        file_path = cur_dir + sep

    # date = datetime.now().strftime("_%Y_%m_%d_%H_%M_%S")
    # file_type = ".xbif"
    # file_name = file_path + file_name + date + file_type
    bn = util.get_bn(ng)
    util.update_bn_positions(bn, ng.graph)

    status = "error"
    if ng.graph_mode[0] == "BN":
        file_type = ".xbif"
        file_name = file_path + file_name + file_type
        XMLBIFParser.write(bn, "".join(file_name), ignoreProperties=False)
        status = "Graph saved as: " + file_name
    elif ng.graph_mode[0] == "DBN":
        dbn = ng.inference_methods.get_dbn()
        DBNSpec.write(dbn, file_path, file_name, ignoreProperties=False)
        status = "Graph saved as: " + file_path + file_name + ".conf"

    app.logger.info(status)
    return jsonify(["", status])

@app.route('/_getGraphString', methods=['GET'])
def get_graph_string():
    bn = util.get_bn(ng)
   
    filecontent = XMLBIFParser.get_string(bn, ignoreProperties=False)
    return jsonify([filecontent, ''])

@app.route('/_getMarkovBlanket', methods=['POST'])
def get_markov_blanket():
    json_request = request.get_json()
    node_name = json_request["nodeName"]

    bn = util.get_bn(ng)
   
    markov_blanket = bn.get_markov_blanket(node_name)
    return jsonify([list(markov_blanket), ''])



@app.route('/_loadGraph', methods=['POST'])
def load_graph():
    json_request = request.get_json()
    content = json_request["content"].encode()
    path = json_request["path"]
    width = json_request["width"]
    height = json_request["height"]

    if content == "NEW":
        ng.new_graph()
        ng.current_layout = "Read"
        ng.width = width
        ng.height = height

        util.set_graph(ng)
        status = "New Graph"
    else:
        try:
            if path[-5:] == ".xbif":
                new_bn = XMLBIFParser.parse(content, ignoreProperties=False)
                ng.new_graph(graph_mode=["BN", ""])
                ng.inference_methods.set_bn(new_bn)
            elif path[-5:] == ".conf":
                new_bn = DBNSpec.parse(content, ignoreProperties=False)
                ng.new_graph(graph_mode=["DBN", "B0"])
                ng.inference_methods.set_dbn(new_bn)
        except:
            # app.logger.info(
            #     "File {} could not be read. Are you sure it is a correct .xbif or .conf file?".format(path))
            status = "Loading failed"
        else:

            ng.current_layout = "Read"
            ng.width = width
            ng.height = height

            util.set_graph(ng)
            status = "New Graph loaded"

    # app.logger.info(status)
    return jsonify([ng.graph, status])


@app.route('/_changeLayout', methods=['POST'])
def change_layout():
    json_request = request.get_json()
    new_layout = json_request["layout"]

    ng.current_layout = new_layout

    util.set_graph(ng)
    ng.current_layout = "Read"
    ng.is_first_position_update = True
    status = "Method changed"

    app.logger.info(status)
    return jsonify([ng.graph, status])


@app.route('/_setGraphMode', methods=['POST'])
def set_graph_mode():
    json_request = request.get_json()
    graph_mode = json_request["graphMode"]

    if graph_mode[0] == ng.graph_mode[0]:
        if graph_mode[1] == "TBN":
            tbn = ng.inference_methods.get_tbn()
            b0 = ng.inference_methods.get_b0()
            if len(tbn.node_lookup) == 0 and len(b0.node_lookup) > 0:
                ng.inference_methods.set_tbn(b0)
            if ng.inference_methods.get_t() == 0:
                ng.inference_methods.unroll()
        ng.graph_mode = graph_mode

    else:
        bn = util.get_bn(ng)
        ng.new_graph(graph_mode=graph_mode)
        ng.current_layout = "Read"
        util.set_bn(ng, bn)

    util.set_graph(ng)
    status = "Method changed"

    app.logger.info(status)
    return jsonify([ng.graph, status])


@app.route('/_unrollDBN', methods=['POST'])
def unroll_dbn():
    json_request = request.get_json()
    steps = int(json_request["steps"])
    reset = bool(json_request["reset"])

    status = "error"
    if ng.graph_mode[0] == "DBN":
        if reset:
            ng.inference_methods.reset()
            status = "Reset timesteps t=0"
            if ng.graph_mode[1] == "TBN":
                ng.inference_methods.unroll()
                status = "Reset timesteps t=1 (TBN-Mode)"
        else:
            for i in range(steps):
                ng.inference_methods.unroll()
            status = "Next timestep" + str(ng.inference_methods.get_t())

    util.set_graph(ng)

    app.logger.info(status)
    return jsonify([ng.graph, status])


