from flask import Flask

app = Flask(__name__, static_url_path='',
                  static_folder='./primo_gui_react/build',
                  template_folder='./primo_gui_react/build')

from . import views
