from primo2.inference.exact import FactorTree
from primo2.inference.exact import VariableElimination
import primo2.inference.mcmc as mcmc
from primo2.inference import dynamic
from primo2 import networks
import functools
import copy

class InferenceMethodsBN:
    def __init__(self):
        self._bn = networks.BayesianNetwork()
        self._bn.name = 'bayesiannetwork'
        self._evidence = {}

        self.__update()

    def __update(self):
        self._tree = FactorTree.create_jointree(self._bn)
        self._tree.set_evidence(self._evidence)
        self._mcmcGibbs = mcmc.MCMC(self._bn, transitionModel=mcmc.GibbsTransition())
        self._mcmcMetropolis = mcmc.MCMC(self._bn, transitionModel=mcmc.MetropolisHastingsTransition())
        self._inference = {
            "Variable Elimination": functools.partial(VariableElimination.bucket_marginals,
                                                      self._bn, evidence=self._evidence),
            "Factor Tree": self._tree.marginals,
            "Gibbs Sampling": functools.partial(self._mcmcGibbs.marginals, evidence=self._evidence),
            "Metropolis Hasting": functools.partial(self._mcmcMetropolis.marginals, evidence=self._evidence),
            "Visualise Samples": functools.partial(self._mcmcGibbs.marginals, stepByStep=True, evidence=self._evidence)}

    def get_bn(self):
        return self._bn

    def set_bn(self, bn):
        self._bn = copy.deepcopy(bn)
        self._bn.name = 'bayesiannetwork'
        self.__update()

    def get_evidence(self):
        return self._evidence

    def set_evidence(self, evidence):
        self._evidence = evidence
        self.__update()

    def get_inference(self):
        return self._inference

    def get_inference_types(self):
        return list(self._inference.keys())

    def infer(self, inference_method, nodes):
        return self._inference[inference_method](nodes)



class InferenceMethodsDBN:
    def __init__(self):
        self._dbn = networks.DynamicBayesianNetwork()
        self._dbn.b0.name = 'dynamicbayesiannetwork_b0'
        self._dbn.two_tbn.name = 'dynamicbayesiannetwork_2tbn'
        self._evidence = {}
        self.__update()

    def __update(self):
        self._pfb = dynamic.PriorFeedbackExact(self._dbn)
        self._pfb.set_evidence(evidence=self._evidence)
        self._se = dynamic.SoftEvidenceExact(self._dbn)
        self._se.set_evidence(evidence=self._evidence)
        self._inference = {
            'Prior Feedback': self._pfb.marginals,
            'Soft Evidence': self._se.marginals}

    def get_dbn(self):
        return self._dbn

    def set_dbn(self, dbn):
        self._dbn = copy.deepcopy(dbn)
        self._dbn.b0.name = 'dynamicbayesiannetwork_b0'
        self._dbn.two_tbn.name = 'dynamicbayesiannetwork_2tbn'
        self.__update()

    def get_b0(self):
        return self._dbn.b0

    def set_b0(self, b0):
        self._dbn.b0 = copy.deepcopy(b0)
        self._dbn.b0.name = 'dynamicbayesiannetwork_b0'
        self.__update()

    def get_tbn(self):
        return self._dbn.two_tbn

    def set_tbn(self, tbn):
        self._dbn.two_tbn = copy.deepcopy(tbn)
        self._dbn.two_tbn.name = 'dynamicbayesiannetwork_2tbn'
        self.__update()

    def get_evidence(self):
        return self._evidence

    def set_evidence(self, evidence):
        self._evidence = evidence
        self._pfb.set_evidence(evidence=self._evidence)
        self._se.set_evidence(evidence=self._evidence)

    def get_inference(self):
        return self._inference

    def get_inference_types(self):
        return list(self._inference.keys())

    def infer(self, inference_method, node):
        return self._inference[inference_method]([node])

    def get_t(self):
        return self._pfb._t

    def unroll(self):
        self._pfb.unroll(evidence=self._evidence)
        self._se.unroll(evidence=self._evidence)

    def reset(self):
        self._evidence = {}
        self.__update()
