import numpy as np
import random

LAYOUT_TYPES = ["Read", "Deep", "Wide"]


def check_new_edge(bn, ng, from_name, to_name):
    edges = ng.graph["links"]
    nodes = ng.graph["nodes"]
    is_to_from = False
    for edge in edges:
        if (nodes[edge["source"]]["name"] == from_name) & (nodes[edge["target"]]["name"] == to_name):
            return 1
        elif (nodes[edge["source"]]["name"] == to_name) & (nodes[edge["target"]]["name"] == from_name):
            is_to_from = True
            break
    find_name = {to_name}
    tmp_set = set(bn.node_lookup[from_name].parents)

    checked_set = set(bn.node_lookup[from_name].parents)
    while len(tmp_set) > 0:
        tmp_node = tmp_set.pop()
        tmp_parents = set(bn.node_lookup[tmp_node].parents)
        if not tmp_parents.isdisjoint(find_name):
            return 3
        tmp_parents.discard(checked_set)
        checked_set.union(tmp_parents)
        tmp_set.union(tmp_parents)

    if is_to_from:
        return 2
    else:
        return 0


def get_bn(ng):
    if ng.graph_mode[0] == "DBN":
        if ng.graph_mode[1] == "TBN":
            return ng.inference_methods.get_tbn()
        else:
            return ng.inference_methods.get_b0()
    else:
        return ng.inference_methods.get_bn()


def get_center_positions(nodes, width, height):
    positions = {}
    for node in nodes:
        positions[node.name] = [width/2, height/2]
    return positions


def get_colspans(node):
    colspans = [1]
    for parent_name in node.parentOrder:
        parent_value_len = len(node.parents[parent_name].values)
        colspans.append(colspans[-1] * parent_value_len)
    colspans = list((colspans[-1] / np.array(colspans)))
    colspans[0] += 1
    return colspans


def get_deepth_layout_positions(info, bn_nodes, width, height, node_radius):
    positions = get_center_positions(bn_nodes, width, height)
    # freq = np.bincount([i["deepth"] for i in info.values()])
    # max_width = max(freq)
    # step_width = float(width / (max_width + 2))
    distance = float(node_radius * 2 * 2)

    for d in range(len(info)):
        new_list = [node for node in bn_nodes if info[node.name]["deepth"] == d]

        if len(new_list) == 0:
            break

        # if step_width < distance:
        #     step_width = float(distance)

        if d > 0:
            weight_list = {}
            for node in new_list:
                weight_list[node.name] = np.mean([positions[parent][0] for parent in node.parentOrder])
            new_list = sorted(new_list, key=lambda n: weight_list[n.name])

        for i, node in enumerate(new_list):
            positions[node.name] = [float(node_radius * 2 + distance * i),                          # x
                                    float(node_radius * 2 + distance * info[node.name]["deepth"])]  # y
    return positions


def get_edge(bn, edge_id):
    all_nodes = bn.get_all_nodes()
    from_to = [all_nodes[int(s)].name for s in edge_id[1:].split("t", 1) if s.isdigit()]
    return from_to


def get_information(bn, f1=0):
    info = dict()
    edges = bn.graph.edge
    nodes = bn.get_all_nodes()
    for node in nodes:
        node_dict = dict()
        node_dict["nr_children"] = len(edges[node.name])
        node_dict["nr_parents"] = len(node.parents)
        if node_dict["nr_parents"] == 0:
            node_dict["deepth"] = 0
        else:
            node_dict["deepth"] = -1
        info[node.name] = node_dict

    for d in range(len(info)):
        new_list = [node for node in nodes if info[node.name]["deepth"] == d]
        if not len(new_list) > 0:
            if d == 0:
                # no parent -> circular properties
                return False
            break

        for node in new_list:
            for child in edges[node.name]:
                if info[child]["deepth"] < ((d+1)*f1):   # Deep if f1 is 1 and wide if f1 = 0
                    info[child]["deepth"] = d + 1
    return info


def get_meta_position(meta):
    tmp_str = [m.split('position = (')[1] for m in meta if m.split()[0] == "position"]
    if len(tmp_str) == 1:
        tmp_str = tmp_str[0][:-1].split(',')
        return [float(tmp_str[0]), float(tmp_str[1])]
    return False


def get_node_copy(graph, node_name):
    for node in graph["nodes"]:
        if node["name"] == node_name:
            return node.copy()
    return None


def get_node_dict(node, ng, inferation=None, position=None):
    if position is None:
        position = [0, 0]
    json_node = {"name": node.name,
                 "table": node.cpd.tolist(),
                 "parents_name": node.parentOrder,
                 "colspans": get_colspans(node),
                 "valid": node.valid,
                 "x": position[0],
                 "y": position[1]}
    '''
    if ng.graph_mode == ["DBN", "TBN"]:
        tbn = ng.inference_methods.get_tbn()
        transitions = ng.inference_methods.get_dbn().transitions
        bn_1 = [t[0] for t in transitions]
        if node.name in bn_1:
            json_node["type"] = "bn_1"
            childs_len = len(tbn.get_children(node.name))
            parents_len = len(node.parents)
            if childs_len > 1 or parents_len > 0:
                json_node["edge_conflict"] = True
            else:
                json_node["edge_conflict"] = False
    else:
    '''
    json_node["type"] = "bn"
    json_node["edge_conflict"] = False

    evidence = ng.inference_methods.get_evidence()
    if evidence.get(node.name, False):
        json_node["evidence"] = evidence[node.name]
    else:
        json_node["evidence"] = ""

    colors = get_colors(get_bn(ng), node.name)
    outcomes = get_bn(ng).node_lookup[node.name].values
    if ng.current_inference_method is not "":
        if ng.current_inference_method == "Visualise Samples":
            json_node["outcomes"] = [{"name": outcomes[i], "value": inferation[i].tolist(), "color": colors[outcomes[i]]}
                                    for i in range(len(outcomes))]
        else:
            inferation = ng.inference_methods.infer(ng.current_inference_method, [node.name])
            json_node["outcomes"] = [{"name": outcomes[i], "value": inferation.potentials[i].tolist(), "color": colors[outcomes[i]]}
                                    for i in range(len(outcomes))]
    else:
        # if (initial-) mode could be empty or inferation failed then
        # values = 1/len(node.values) or values = 1 | 0 if evidence is available
        if json_node["evidence"] == "":
            json_node["outcomes"] = [{"name": o, "value": 1/len(node.values), "color": colors[o]} for o in node.values]
        else:
            json_node["outcomes"] = [{"name": o, "value": float(o == json_node["evidence"]), "color": colors[o]} for o in node.values]

    return json_node

def get_positions(bn, ng):
    nodes = bn.get_all_nodes()
    '''
    if ng.current_layout == "Wide":
        info = get_information(bn, f1=0)
        if not info:
            return get_center_positions(nodes, ng.width, ng.height)
        positions = get_deepth_layout_positions(info, nodes, ng.width, ng.height, ng.node_radius)
        return positions

    elif ng.current_layout == "Deep":
        info = get_information(bn, f1=1)
        if not info:
            return get_center_positions(nodes, ng.width, ng.height)
        positions = get_deepth_layout_positions(info, nodes, ng.width, ng.height, ng.node_radius)
        return positions

    elif ng.current_layout == "Read":
        positions = {}
        position_list = []
        for node in nodes:
            position = get_meta_position(node.meta)
            if not position:
                return get_center_positions(nodes, ng.width, ng.height)
            position_list.append(position)

        add_xy = [ng.node_radius * 2, ng.node_radius * 2] - np.array(min(position_list))

        for i, node in enumerate(nodes):
            positions[node.name] = [max(position_list[i][0], position_list[i][0] + add_xy[0]),
                                    max(position_list[i][1], position_list[i][1] + add_xy[1])]
        return positions

    else:
        return get_center_positions(nodes, ng.width, ng.height)
    '''
    positions = {}
    for node in nodes:
        position = get_meta_position(node.meta)
        if not position:
          set_node_position(ng, node.name, ng.width/2, ng.height/2)
          position = get_meta_position(node.meta)

        positions[node.name] = position

    return positions
    

def set_bn(ng, bn):
    if ng.graph_mode[0] == "DBN":
        if ng.graph_mode[1] == "TBN":
            ng.inference_methods.set_tbn(bn)
        else:
            ng.inference_methods.set_b0(bn)
    else:
        ng.inference_methods.set_bn(bn)


def set_graph(ng):
    bn = get_bn(ng)

    json_graph = ng.graph
    nodes = bn.get_all_nodes()
    edges = bn.graph.edges()

    json_graph["graphMode"] = ng.graph_mode
    json_graph["directory"] = ng.dir
    json_graph["inference_types"] = ng.inference_methods.get_inference_types()
    json_graph["current_inference_method"] = ng.current_inference_method
    json_graph["layout_types"] = LAYOUT_TYPES
    json_graph["links"] = [{"source": list(nodes).index(edge[0]), "target": list(nodes).index(edge[1])} for edge in edges]

    json_graph["time"] = ng.inference_methods.get_t() if ng.graph_mode[0] == "DBN" else 0

    if len(nodes) > 0:
        positions = get_positions(bn, ng)
        if ng.current_inference_method == "Visualise Samples":
            inferation = ng.inference_methods.infer(ng.current_inference_method, [node.name for node in nodes])
            json_graph["nodes"] = [get_node_dict(node, ng, inferation[node.name], positions[node.name]) for node in nodes]
        else:
            json_graph["nodes"] = [get_node_dict(node, ng, position=positions[node.name]) for node in nodes]
    else:
        json_graph["nodes"] = []
        
    ng.graph = json_graph
    

def set_node_position(graph, node_name, x, y, bn=None):

    for node in graph["nodes"]:
        if node["name"] == node_name:
            node["x"] = x
            node["y"] = y
            break
    if bn:
        pos = "position = (" + str(x) + "," + str(y) + ")"
        is_existing = False
        for i, m in enumerate(bn.node_lookup[node_name].meta):
            if m.split()[0] == "position":
                bn.node_lookup[node_name].meta[i] = pos
                is_existing = True
        if not is_existing:
            bn.node_lookup[node_name].meta.append(pos)


def update_bn_positions(bn, graph):
    for node in graph["nodes"]:
        pos = "position = (" + str(node["x"]) + "," + str(node["y"]) + ")"
        is_existing = False
        for i, m in enumerate(bn.node_lookup[node["name"]].meta):
            if m.split()[0] == "position":
                bn.node_lookup[node["name"]].meta[i] = pos
                is_existing = True
        if not is_existing:
            bn.node_lookup[node["name"]].meta.append(pos)

def get_colors(bn, node_name):
    meta = bn.node_lookup[node_name].meta
    tmp_str = [m.split('colors = (')[1] for m in meta if m.split()[0] == "colors"]
    if len(tmp_str) == 1:
        tmp_str = tmp_str[0][:-1].split(',')
        return {entry.split(':')[0]: entry.split(':')[1] for entry in tmp_str}
    else:
        default_colors = ['#008000', '#FF0000', '#0000FF', '#FFFF00', '#00FFFF', '#FF00FF', '#800000', '#808000', '#00FF00', '#C0C0C0']
        colors_prop = "colors = ("
        colors = dict()
        for i, value in enumerate(bn.node_lookup[node_name].values):
          colors_prop = colors_prop + value + ':'
          if i < len(default_colors):
            color = default_colors[i]
          else:
            color = '#'.join(random.choice('0123456789abcdef') for n in range(6))
          colors[value] = color
          colors_prop += color
          colors_prop += ","
        colors_prop = colors_prop[:-1] + ")"
        bn.node_lookup[node_name].meta.append(colors_prop)
        return colors

def update_colors(bn, node_name, new_value_names, new_value_colors):
    colors_prop = "colors = ("
    for i, value_name in enumerate(new_value_names):
      colors_prop = colors_prop + value_name + ':' + new_value_colors[i] + ','
    colors_prop = colors_prop[:-1] + ")"

    for i, m in enumerate(bn.node_lookup[node_name].meta):
        if m.split()[0] == "colors":
            bn.node_lookup[node_name].meta[i] = colors_prop